from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import Kegiatan_Form, Register_Form
from .models import Kegiatan, Register
from datetime import datetime
import json

# Create your views here.
mhs_name = 'Gusti Ngurah Agung Satya Dharma'
desc = '''Selamat datang di profile saya. Nama saya adalah Gusti
            Ngurah Agung Satya Dharma, dengan nama panggilan
            Satya. Saya tinggal di Jakarta dan sekarang sedang menjalani
            kuliah di Fasilkom UI dalam jurusan Sistem Informasi.
            Saya menganggap diri saya sebagai seseorang yang mampu
            bekerja keras dan rela untuk bekerjasama dengan semua jenis
            orang. Keahlian saya berada dalam programming.'''
pendidikan1 = 'SD Kembang         2005 - 2011'
pendidikan2 = 'SMP Pangudi Luhur    2011- 2014'
pendidikan3 = 'SMA Gonzaga          2014 - 2017'
pendidikan4 = 'Universitas Indonesia    2017 - Current'


def index(request):
    response = {'name': mhs_name, 'desc': desc, 'pendidikan1': pendidikan1, 'pendidikan2': pendidikan2,
                'pendidikan3': pendidikan3, 'pendidikan4': pendidikan4}
    return render(request, 'index.html', response)


def schedules(request):
    if (request.method == 'POST'):
        form = Kegiatan_Form(request.POST)
        if (form.is_valid()):
            status = request.POST['status']
            kegiatan = Kegiatan(status=status, date=datetime.now())
            kegiatan.save()
        else:
            return HttpResponse('Something wrong happened, try again')
    else:
        form = Kegiatan_Form()
    html = 'schedule.html'
    all_p = Kegiatan.objects.all()

    return render(request, html, {'form': form, 'objects': all_p})


def book_list(request):
    return render(request, 'book_list.html')


def register(request):
    if request.method == "POST":
        form = Register_Form(request.POST)
        if form.is_valid():
            name = request.POST['name']
            email = request.POST['email']
            password = request.POST['password']
            register = Register(name=name, email=email, password=password)
            register.save()
            return redirect('register')
    else:
        form = Register_Form()

    return render(request, 'regForm.html', {'form': form})


def validate_register(request):
    email = request.GET.get('email')
    data = {
        'is_taken': Register.objects.filter(email__iexact=email).exists()
    }
    print(email)
    if data['is_taken']:
        data['error'] = "An User with this email already exist!"
    return JsonResponse(data)


def sub_list(request):
    raw = Register.objects.all()
    jlist = [{'name': obj.name, 'email': obj.email} for obj in raw]
    return JsonResponse(jlist, safe=False)


def subscriptions(request):
    return render(request, 'subs.html')


def unsub(request,email):
    Register.objects.filter(email__iexact=email).delete()
    return HttpResponse("OK")