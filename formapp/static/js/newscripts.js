function register_form_validation(bool) {
    $("#form").validate();
    return $("#form").valid();
}

function toggle_disable_button(bool) {
    if (bool) {
        $("#submit").prop('disabled', false);
    } else {
        $("#submit").prop('disabled', true);
    }
}

$(function(){
    toggle_disable_button(false);
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $("#form").change(function() {
        toggle_disable_button(false);
        $("#id_name").change(function() {
            toggle_disable_button(register_form_validation());
        })

        $("#id_password").change(function() {
            toggle_disable_button(register_form_validation());
        })

        var email = $("#id_email").val();
        $.ajax({
            url: urlvalid,
            data: {
                'email' : email,
            },
            dataType : 'json',
            success: function(data) {
                toggle_disable_button(register_form_validation() && !data.is_taken);
                if (data.is_taken && !document.getElementById("emailtaken")) {
                    $("#id_email").after("<p id='emailtaken' >Email is taken by another user!</p>");
                } else {
                    if (!data.is_taken) {
                        $("#emailtaken").remove();
                    }
                }
            }
        });
    });

    $('#form').on('submit', function(event){
        event.preventDefault();
        create_account();
    });

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function create_account() {
        console.log("create account is working!") // sanity check
        var name = $("#id_name").val();
        var email = $('#id_email').val();
        var password = $('#id_password').val();

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $.ajax({
            url : register, // the endpoint
            type : "POST", // http method
            data : { 'name' : name, 'email' : email, 'password' : password }, // data sent with the post request
            // handle a successful response
            success : function(data) {
                alert("You are successfully registered!, " + name);
            },
            error : function(data) {
                alert("There's an error going on!");
            }
        });
    };


});