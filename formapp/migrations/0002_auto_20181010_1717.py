# Generated by Django 2.1.1 on 2018-10-10 10:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kegiatan',
            name='hari',
        ),
        migrations.RemoveField(
            model_name='kegiatan',
            name='jam',
        ),
        migrations.RemoveField(
            model_name='kegiatan',
            name='kategori',
        ),
        migrations.RemoveField(
            model_name='kegiatan',
            name='namaKeg',
        ),
        migrations.RemoveField(
            model_name='kegiatan',
            name='tanggal',
        ),
        migrations.RemoveField(
            model_name='kegiatan',
            name='tempat',
        ),
        migrations.AddField(
            model_name='kegiatan',
            name='date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='kegiatan',
            name='status',
            field=models.CharField(default='Nothing much', max_length=300),
        ),
    ]
