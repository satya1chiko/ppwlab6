from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    status = models.CharField(max_length=300, default='Nothing much')
    date = models.DateTimeField(auto_now=True)

class Register(models.Model):
    name = models.CharField(max_length=70)
    email = models.EmailField(max_length=200)
    password = models.CharField(max_length=30)
