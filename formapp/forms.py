from django import forms


class Kegiatan_Form(forms.Form):
    status = forms.CharField(max_length=300,label='Nama', widget=forms.TextInput(attrs={'class': 'form-control'}))

class Register_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input yang kosong',
        'invalid': 'Isi input dengan benar',
    }

    name = forms.CharField(label='Nama', required=True, max_length=70,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True,max_length=200, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=30, min_length=8, required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))
