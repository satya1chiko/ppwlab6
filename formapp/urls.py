from django.urls import path
from .views import *

urlpatterns = [
    path('', schedules, name='schedules'),
    path('myprofile/', index, name='index'),
    path('booklist/',book_list, name='book_list'),
    path('register/',register, name='register'),
    path('register/validation/', validate_register, name="validate_register"),
    path('subscriptions/list', sub_list, name='sub_list'),
    path('subscriptions/', subscriptions, name='subscription'),
    path('subscriptions/unsub/<email>', unsub, name='unsub')
]