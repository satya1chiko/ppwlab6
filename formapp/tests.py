from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Kegiatan, Register
from django.http import HttpRequest
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest


# Create your tests here.

class Lab6UnitTest(TestCase):
    def test_landing_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_schedules_func(self):
        found = resolve('/')
        self.assertEqual(found.func, schedules)

    def test_models_work(self):
        Kegiatan.objects.create(status="AAAAAAAAAAAAAAAAA", date=datetime.now())
        amount = Kegiatan.objects.all().count()
        self.assertEqual(amount, 1)
        Register.objects.create(name="satya", email="satya1chiko@gmail.com", password="GIORNOLIKESPISS")
        amount2 = Register.objects.all().count()
        self.assertEqual(amount2,1)

    def test_using_schedule_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'schedule.html')

    def test_landing_page_has_header(self):
        request = HttpRequest()
        response = schedules(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', html_response)

    # Challenge
    def test_profile_page_exist(self):
        response = Client().get('/myprofile/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/myprofile/')
        self.assertEqual(found.func, index)

    def test_has_elements_of_profile(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>' + 'My Profile' + '</title>', html_response)
        self.assertIn('Nama : ' + mhs_name, html_response)
        self.assertIn('Tempat Lahir :', html_response)
        self.assertIn('Tanggal Lahir :', html_response)
        self.assertIn(desc, html_response)
        self.assertFalse(len(mhs_name) == 0)

    def test_using_index_template(self):
        response = Client().get('/myprofile/')
        self.assertTemplateUsed(response, 'index.html')

    # Story 8
    def test_using_book_list_template(self):
        response = Client().get('/booklist/')
        self.assertTemplateUsed(response, 'book_list.html')

    # Story 10
    def test_using_regForm_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'regForm.html')

    def test_using_subs_template(self):
        response = Client().get('/subscriptions/')
        self.assertTemplateUsed(response, 'subs.html')

# Story 7
# class Lab7UnitTest(TestCase):
# def setUp(self):
# chrome_options = Options()
# chrome_options.add_argument('--dns-prefetch-disable')
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('disable-gpu')
# self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# super(Lab7UnitTest, self).setUp()

# def tearDown(self):
# self.selenium.quit()
# super(Lab7UnitTest, self).tearDown()

# def test_add_status_in_selenium(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/")

# status = selenium.find_element_by_name("status")
# submit = selenium.find_element_by_name("submit_button")

# status.send_keys('Coba Coba')
# submit.submit()

# text = selenium.find_elements_by_class_name("df")
# assert "Coba Coba" in text[-1].text

# def test_check_layout_1(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/")

# self.assertEquals("Landing Page", selenium.title)

# def test_check_layout_2(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/")

# self.assertIn("Hello, Apa Kabar?", selenium.page_source)

# def test_check_CSS_1(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/")

# prof = selenium.find_element_by_link_text("My Profile")
# prof.click()

# self.assertNotIn("Hello, Apa Kabar?", selenium.page_source)

# def test_check_CSS_2(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/myprofile/")

# home = selenium.find_element_by_id("selector")
# css = home.value_of_css_property("font-family")

# self.assertEquals("Lato", css)

# Story 8
# def test_check_CSS_theme_change(self):
# selenium = self.selenium
# selenium.get("https://ppwisfunp2.herokuapp.com/myprofile/")

# prof = selenium.find_element_by_name("changetheme")
# prof.click()

# body = selenium.find_element_by_tag_name("body")
# css = body.value_of_css_property("background-color")

# self.assertEquals("black", css)
